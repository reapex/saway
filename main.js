const Discord = require('discord.js');
const bot = new Discord.Client();
var mysql = require('mysql');
const commandeChannel = "734373148983558165"

// prefix :
let prefix = "?";

var sql = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "mydb"
});

bot.on('ready', () => {
    console.log(`Logged in as ${bot.user.tag}!`);

    sql.connect(function (err) {
        if (err) throw err;
        console.log(`Connecté à la BDD !`);
    });

    bot.user.setActivity('twitter/sawayseller', { type: 'WATCHING' })

});

bot.on('message', msg => {

    if (msg.author.bot) return;

    function isAdmin(msg) {
        if (!msg.member.hasPermission("ADMINISTRATOR")) {
            return false
        }
    }

    function createEmbed(texte, color) {

        if (!color) {
            color = '#0099E1'
        }
        if (color == "vert") {
            color = "#3dad26"
        }
        if (color == "orange") {
            color = "#db9c1f"
        }
        if (color == "rouge") {
            color = "#bf3a2e"
        }

        embed = new Discord.RichEmbed()
            .setDescription(texte)
            .setColor(color)

        msg.channel.send(embed).then(e => e.delete(15000))
    }

    function createTable(msg, name) {
        sql.query(`CREATE TABLE IF NOT EXISTS ${name} (id int(11) NOT NULL AUTO_INCREMENT, code VARCHAR(255) NOT NULL, PRIMARY KEY (id))`, function (err, result) {
            if (err) {
                createEmbed(`Impossible de créer cette table.`, 'rouge')
            }
            else {
                createEmbed(`La table **${name}** a bien été créée. Afficher les tables: **${prefix}table**`, 'vert')
            }
        });
    }

    function insererProduit(msg, categorie, produit) {
        sql.query(`INSERT INTO ${categorie} (code) VALUES ("${produit}")`, function (err, result) {
            if (err) {
                createEmbed(`La catégorie spécifiée n'existe pas...`, 'rouge')
            }
            else {
                createEmbed(`Le produit **${produit}** a correctement été ajouté.`, 'vert')
            }
        });
    }

    function give(msg, giveCategorie, userID) {

        msg.delete()

        if (!giveCategorie || !userID) {
            return createEmbed("Pas de catégorie ou d'utilisateur choisi. Annulation...", 'orange')
        }

        sql.query(`SELECT code FROM ${giveCategorie} limit 1`, function (err, result) {
            if (err) {
                createEmbed("Une erreur est survenue.", 'rouge')
            }
            else if (result[0] == undefined) {
                return createEmbed("Rien a envoyer. Annulation...", 'orange')
            }
            else {

                userID = bot.users.get(userID)

                embed = new Discord.RichEmbed()
                    .setColor('#0099E1')
                    .addField(`🎁 Vous avez reçu un message !`, `Saway est heureux de vous envoyer un code **${giveCategorie}**, votre code est : **${result[0]['code']}**`)

                userID.send(embed)

                sql.query(`DELETE FROM ${giveCategorie} limit 1`, function (err, result) {
                    if (err) {
                        console.log(err)
                    }
                })

            }

        })
    }

    function registerCommand(msg, contenu) {

        msg.delete()

        let userID = msg.author.id;
        let username = msg.author.username;

        let channelToSend = bot.channels.get('734373148983558165')

        embed = new Discord.RichEmbed()
            .setColor('#0099E1')
            .addField(`📌Nouvelle commande !`, `⚠️**${username}** (${userID}) vient de passer une commande:\n👉 ${contenu}`)

        channelToSend.send(embed).then(embedMessage => {
            embedMessage.react("👍");
        })
    }



















































    //////////////////////////////////////////////////////////////////

    // Help
    if (msg.content === prefix + 'help') {
        if (isAdmin(msg) == false) return;
        msg.delete()

        embed = new Discord.RichEmbed()
            .setThumbnail('https://pbs.twimg.com/profile_images/1281760689804255232/C6r-b11G_400x400.jpg')
            .setColor('#0099E1')
            .addField("Modérateur: ", "**?create <nom_de_la_table>**: Créée une table (ex: minty, ikonic, ...)\n**?del <nom_de_la_table>**: Supprime la table.\n**?add <nom_de_la_table> <code_produit>**: Ajoute un produit dans une table.\n**?table**: Affiche les différentes tables.\n**?give <nom_de_la_table> <id_de_la_personne>**: Envoie un code à une personne.")
            .addField("Utilisateur: ", "**?new <message>**: Permet de passer commande. Dans votre message, saisissez entièrement votre choix.")

        msg.channel.send(embed)
    }

    // Create table
    if (msg.content.startsWith(prefix + 'create')) {
        if (isAdmin(msg) == false) return;
        msg.delete()
        database_name = msg.content.split(" ")[1];
        createTable(msg, database_name)
    }

    // Ajouter produit
    if (msg.content.startsWith(prefix + "add")) {
        if (isAdmin(msg) == false) return;
        msg.delete()

        categorie = msg.content.split(" ")[1];
        produit = msg.content.split(" ")[2];

        if (!categorie || !produit) {
            return createEmbed("Aucune catégorie ou aucun produit à ajouter. Annulation...", 'orange')
        }
        else {
            categorie = categorie.toLowerCase();
            insererProduit(msg, categorie, produit);
        }
    }

    // Afficher listes
    if (msg.content == (prefix + "table")) {
        if (isAdmin(msg) == false) return;
        msg.delete()

        sql.query(`SHOW tables`, function (err, result) {
            if (err) {
                createEmbed('Impossible de récuperer les tables.', 'rouge')
            }
            else {
                taille = result.length

                if (taille == 0) {
                    return createEmbed("Aucune table à afficher.", 'orange')
                }

                msg.channel.send("```fix\n📜Tables disponibles:```").then(e => e.delete(30000))

                for (let i = 0; i < taille; i++) {
                    a = (`${i + 1} | ${result[i]['Tables_in_mydb']}`)
                    msg.channel.send("```" + a + "```").then(e => e.delete(30000))
                }
                msg.channel.send("```diff\n- ⚠️Utilisez ?create/?del pour créer/supprimer une table.```").then(e => e.delete(30000))
            }

        })
    }

    //Supprimer liste
    if (msg.content.startsWith(prefix + "del")) {
        if (isAdmin(msg) == false) return;
        msg.delete()
        nomTable = msg.content.split(" ")[1]

        if (!nomTable) {
            return createEmbed("Merci de choisir une table à supprimer.", 'orange')
        }

        sql.query(`DROP TABLE ${nomTable}`, function (err, result) {
            if (err) {
                createEmbed(`Impossible de supprimer la table ${nomTable}`, 'rouge')
            }
            else {
                createEmbed(`La table ${nomTable} a bien été suprimé. Affichez les tables avec **?table**`, 'vert')
            }
        })

    }

    //Donner item
    if (msg.content.startsWith(prefix + "give")) {
        if (isAdmin(msg) == false) return;
        giveCategorie = msg.content.split(" ")[1]
        userID = msg.content.split(" ")[2]

        give(msg, giveCategorie, userID)
    }

    if (msg.content.startsWith(prefix + "new")) {
        if (isAdmin(msg) == false) return;
        contenue = msg.content.slice(5)

        if (!contenue) {
            return createEmbed("Merci de bien vouloir saisir un message", 'orange')
        }
        registerCommand(msg, contenue)
    }

});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////                                                                                                                                                                              //////////
//////////                                                                              Login                                                                                           //////////
//////////                                                                                                                                                                              //////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bot.on('messageReactionAdd', (reaction, user) => {

    if (user.bot) return;
    if (reaction.emoji.name === "👍") {
        if (!reaction.message.channel.id === commandeChannel) {
            return;
        } else {
            reaction.message.delete()

            embed = new Discord.RichEmbed()
                .addField('Commande fermée', `La commande a été fermée par ${user.username} !`)
                .setTimestamp()
                .setColor('#3dad26')

            reaction.message.channel.send(embed).then(e => e.delete(10000))
        }
    }
})

bot.login('config.token');